use super::*;
use crate::ease::Ease;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Fade {
    item: Box<dyn Animated>,
    ease: Ease,
}

impl Fade {
    pub fn new(item: Box<dyn Animated>, ease: Ease) -> Self {
        Fade {
            item,
            ease,
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Fade>();
        engine.register_fn("fade", Fade::new);
        engine.register_fn("as_animated", Fade::as_animated);
    }
}

#[typetag::serde]
impl Animated for Fade {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        context.push_group();
        self.item.draw(context, offset);
        context.pop_group_to_source();

        let time = offset / self.item.duration();
        let mov = self.ease.ease(time);

        context.paint_with_alpha(mov);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.item.audio()
    }
}
