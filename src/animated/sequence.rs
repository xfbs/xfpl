use super::*;

/// Sequence of animations. Basically a container type, which takes a number
/// of animations and plays them in order.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Sequence {
    items: Vec<Box<dyn Animated>>,
    size: Size,
    duration: f64,
}

impl Sequence {
    pub fn new() -> Self {
        Sequence {
            items: Vec::new(),
            size: Size::default(),
            duration: 0f64,
        }
    }

    pub fn add(&mut self, item: Box<dyn Animated>) {
        self.duration += item.duration();

        // update size if we got an item that was bigger.
        if item.size().x > self.size.x {
            self.size.x = item.size().x;
        }

        if item.size().y > self.size.y {
            self.size.y = item.size().y;
        }

        self.items.push(item);
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Sequence>();
        engine.register_fn("sequence", Sequence::new);
        engine.register_fn("add", Sequence::add);
        engine.register_fn("as_animated", Sequence::as_animated);
    }
}

#[typetag::serde(name = "sequence")]
impl Animated for Sequence {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        let mut offset = offset;

        for item in self.items.iter() {
            let duration = item.duration();
            if offset > duration {
                offset -= duration;
            } else {
                item.draw(context, offset);
                break;
            }
        }
    }

    fn size(&self) -> Size {
        self.size
    }

    fn duration(&self) -> f64 {
        self.duration
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        Box::new(std::iter::empty())
    }
}

