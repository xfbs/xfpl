use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Loop {
    item: Box<dyn Animated>,
    amount: usize,
}

impl Loop {
    pub fn new(item: Box<dyn Animated>, amount: usize) -> Self {
        Loop { item, amount }
    }
}

#[typetag::serde]
impl Animated for Loop {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        let offset = offset % self.item.duration();
        self.item.draw(context, offset);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.item.audio()
    }
}

