use super::*;

/// Show a drawable object statically for a specified amount of time.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Static {
    item: Box<dyn Drawable>,
    duration: f64,
}

impl Static {
    pub fn new(item: Box<dyn Drawable>, duration: f64) -> Self {
        Static { item, duration }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Static>();
        engine.register_fn("static", Static::new);
        engine.register_fn("as_animated", Static::as_animated);
    }
}

#[typetag::serde(name = "static")]
impl Animated for Static {
    fn draw(&self, context: &cairo::Context, _offset: f64) {
        self.item.draw(context);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.duration
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        Box::new(std::iter::empty())
    }
}
