use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Audible {
    item: Box<dyn Animated>,
    name: String,
    time: f64,
}

impl Audible {
    pub fn new(item: Box<dyn Animated>, name: String, time: f64) -> Self {
        Audible { item, name, time }
    }
}

#[typetag::serde]
impl Animated for Audible {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        self.item.draw(context, offset);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        let mut audio = AudioItem::new(self.name.clone());
        audio.set_start(self.time);

        Box::new(self.item.audio().chain(std::iter::once(audio)))
    }
}

