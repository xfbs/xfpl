use super::*;
use crate::ease::Ease;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Move {
    item: Box<dyn Animated>,
    to: Size,
    ease: Ease,
}

impl Move {
    pub fn new(item: Box<dyn Animated>, to: Size, ease: Ease) -> Self {
        Move { item, to, ease }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Move>();
        engine.register_fn("move", Move::new);
        engine.register_fn("as_animated", Move::as_animated);
    }
}

#[typetag::serde(name = "move")]
impl Animated for Move {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        let x_off = if self.to.x < 0.0 { -self.to.x } else { 0.0 };
        let y_off = if self.to.y < 0.0 { -self.to.y } else { 0.0 };

        let time = offset / self.item.duration();
        let mov = self.ease.ease(time);

        context.save();
        context.translate(self.to.x * mov + x_off, self.to.y * mov + y_off);
        self.item.draw(context, offset);
        context.restore();
    }

    fn size(&self) -> Size {
        let mut size = self.item.size();

        size.x += self.to.x.abs();
        size.y += self.to.y.abs();

        size
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.item.audio()
    }
}
