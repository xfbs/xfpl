use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Positioned {
    item: Box<dyn Animated>,
    position: Size,
    anchor: Anchor,
}

impl Positioned {
    pub fn new(item: Box<dyn Animated>, position: Size) -> Self {
        Positioned {
            item,
            position,
            anchor: Anchor::North,
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Positioned>();
        engine.register_fn("positioned", Positioned::new);
        engine.register_fn("as_animated", Positioned::as_animated);
    }
}

#[typetag::serde(name = "positioned")]
impl Animated for Positioned {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        context.save();
        context.translate(self.position.x, self.position.y);
        self.item.draw(context, offset);
        context.restore();
    }

    fn size(&self) -> Size {
        Size { x: 1f64, y: 1f64 }
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        Box::new(std::iter::empty())
    }
}
