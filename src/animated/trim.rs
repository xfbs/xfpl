use super::*;

/// Show an animation, stripping a predefined amount off at the end.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Trim {
    item: Box<dyn Animated>,
    strip: f64,
}

impl Trim {
    pub fn new(item: Box<dyn Animated>, strip: f64) -> Self {
        Trim { item, strip }
    }
}

#[typetag::serde(name = "strip")]
impl Animated for Trim {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        self.item.draw(context, offset);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration() - self.strip
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        Box::new(std::iter::empty())
    }
}
