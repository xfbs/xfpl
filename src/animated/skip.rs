use super::*;

/// Shows an Animation, but skipping a predefined amount of seconds.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Skip {
    item: Box<dyn Animated>,
    skip: f64,
}

impl Skip {
    pub fn new(item: Box<dyn Animated>, skip: f64) -> Self {
        Skip { item, skip }
    }
}

#[typetag::serde(name = "skip")]
impl Animated for Skip {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        self.item.draw(context, offset + self.skip);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration() - self.skip
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.item.audio()
    }
}
