use crate::common::*;
use crate::drawable::Drawable;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

mod r#move;
pub use r#move::Move;

mod positioned;
pub use positioned::Positioned;

mod skip;
pub use skip::Skip;

mod r#static;
pub use r#static::Static;

mod trim;
pub use trim::Trim;

mod sequence;
pub use sequence::Sequence;

mod scene;
pub use scene::Scene;
pub use scene::SceneItem;

mod r#loop;
pub use r#loop::Loop;

mod audible;
pub use audible::Audible;

mod transparent;
pub use transparent::Transparent;

mod ttyrec;
pub use self::ttyrec::TTYRec;

mod fade;
pub use fade::Fade;

/// Trait for something that can be drawn and is animated. Takes an offset,
/// which is the time in seconds that should be drawn right now. Also has a
/// duration, which is how long it is supposed to be shown for.
#[typetag::serde(tag = "animated")]
pub trait Animated: CloneAnimated + Debug {
    fn draw(&self, context: &cairo::Context, offset: f64);
    fn size(&self) -> Size;
    fn duration(&self) -> f64;
    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>>;
}

pub trait CloneAnimated {
    fn to_animated(&self) -> Box<dyn Animated>;
    fn as_animated(self) -> Box<dyn Animated>;
}

impl<T> CloneAnimated for T
where
    T: Animated + Clone + 'static,
{
    fn to_animated(&self) -> Box<dyn Animated> {
        Box::new(self.clone())
    }

    fn as_animated(self) -> Box<dyn Animated> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Animated> {
    fn clone(&self) -> Self {
        (*self).to_animated()
    }
}

pub fn register_all(engine: &mut rhai::Engine) {
    Sequence::register(engine);
    Static::register(engine);
    Move::register(engine);
    Positioned::register(engine);
    Scene::register(engine);
    TTYRec::register(engine);
    Transparent::register(engine);
    Fade::register(engine);
}

/*
pub struct FadeIn {
    item: Box<dyn Drawable>,
    duration: f64,
}

pub struct FadeOut {
    item: Box<dyn Drawable>,
    duration: f64,
}

*/
