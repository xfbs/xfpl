use super::*;
use std::rc::Rc;
use std::cell::RefCell;
use ::ttyrec::Frame;

/// Show a drawable object statically for a specified amount of time.
#[derive(Serialize, Deserialize)]
pub struct TTYRec {
    data: Vec<u8>,
    duration: f64,

    #[serde(skip)]
    parsed: RefCell<Option<Vec<Frame>>>,

    #[serde(skip)]
    pos: RefCell<Option<f64>>,

    #[serde(skip)]
    state: RefCell<Option<vt100::Parser>>,
}

impl Clone for TTYRec {
    fn clone(&self) -> Self {
        TTYRec {
            data: self.data.clone(),
            duration: self.duration,
            parsed: Default::default(),
            pos: Default::default(),
            state: Default::default(),
        }
    }
}

impl std::fmt::Debug for TTYRec {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TTYRec")
         .field("data", &self.data)
         .field("duration", &self.duration)
         .finish()
    }
}

impl TTYRec {
    pub fn new<D: Into<Vec<u8>>>(data: D) -> Self {
        let data = data.into();

        // parse data initially
        let mut parser = ::ttyrec::Parser::new();
        parser.add_bytes(&data);

        let mut last_frame = None;

        loop {
            if let Some(frame) = parser.next_frame() {
                last_frame = Some(frame);
            } else {
                break;
            }
        }

        let duration = match last_frame {
            None => 0f64,
            Some(frame) => (frame.time - parser.offset().unwrap()).as_secs_f64(),
        };

        TTYRec {
            data: data.into(),
            duration,
            parsed: Default::default(),
            pos: Default::default(),
            state: Default::default(),
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        use rhai::RegisterResultFn;
        engine.register_type::<TTYRec>();
        engine.register_result_fn("ttyrec", TTYRec::from_file);
        engine.register_fn("as_animated", TTYRec::as_animated);
    }

    pub fn from_file(file: Rc<std::fs::File>) -> Result<Self, Box<rhai::EvalAltResult>> {
        use std::io::Read;
        file.try_clone()
            .map_err(|e| e.to_string().into())
            .and_then(|mut f| {
                let mut vec = Vec::new();
                f.read_to_end(&mut vec)
                    .map_err(|e| e.to_string().into())
                    .map(|_| vec)
            })
            .map(|data| TTYRec::new(data))
    }

    fn frames(&self) {
        if self.parsed.borrow().is_none() {
            let mut frames = Vec::new();

            let mut parser = ::ttyrec::Parser::new();
            parser.add_bytes(&self.data);

            loop {
                if let Some(frame) = parser.next_frame() {
                    frames.push(frame);
                } else {
                    break;
                }
            }

            // fix duration
            if let Some(offset) = parser.offset() {
                for frame in &mut frames {
                    frame.time -= offset;
                }
            }

            *self.parsed.borrow_mut() = Some(frames);
        }
    }
}

#[typetag::serde]
impl Animated for TTYRec {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        self.frames();
        let borrowed = self.parsed.borrow();
        let frames = borrowed.as_ref().unwrap();

        // ensure we have a state.
        if self.state.borrow().is_none() {
            *self.state.borrow_mut() = Some(vt100::Parser::new(80, 24, 0));
        }

        let mut borrowed_state = self.state.borrow_mut();
        let state: &mut Option<vt100::Parser> = &mut *borrowed_state;
        let state: Option<&mut vt100::Parser> = state.into();
        let state = state.unwrap();

        let prev = *self.pos.borrow();
        frames.iter()
            .skip_while(|f| {
                let time = f.time.as_secs_f64();
                prev.map(|p| time <= p).unwrap_or(false)
            })
            .take_while(|f| f.time.as_secs_f64() <= offset)
            .for_each(|x| {
                (*state).process(&x.data);
            });

        *self.pos.borrow_mut() = Some(offset);

        println!("{}", state.screen().contents());

        let text = crate::drawable::Text::new(
            state.screen().contents(),
            "SF Mono".into(),
            0.01f64,
            RGBA::new(0f64, 0f64, 0f64, 1f64));

        text.draw(context);
    }

    fn size(&self) -> Size {
        Size::default()
    }

    fn duration(&self) -> f64 {
        self.duration
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        Box::new(std::iter::empty())
    }
}
