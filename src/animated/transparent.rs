use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Transparent {
    item: Box<dyn Animated>,
    alpha: f64,
}

impl Transparent {
    pub fn new(item: Box<dyn Animated>, alpha: f64) -> Self {
        Transparent {
            item,
            alpha,
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Transparent>();
        engine.register_fn("transparent", Transparent::new);
        engine.register_fn("as_animated", Transparent::as_animated);
    }
}

#[typetag::serde]
impl Animated for Transparent {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        context.push_group();
        self.item.draw(context, offset);
        context.pop_group_to_source();
        context.paint_with_alpha(self.alpha);
    }

    fn size(&self) -> Size {
        self.item.size()
    }

    fn duration(&self) -> f64 {
        self.item.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.item.audio()
    }
}
