use crate::movie::Movie;
use crate::Subcommand;
use clap::{App, Arg, ArgMatches};
use libflate::gzip::Decoder;
use std::error::Error;
use std::fs::OpenOptions;
use std::path::PathBuf;

pub struct Dump {
    file: PathBuf,
}

pub fn load_movie(path: &PathBuf) -> Result<Movie, Box<dyn Error>> {
    let file = OpenOptions::new().read(true).open(path)?;

    let decoder = Decoder::new(file)?;

    let movie: Movie = bincode::deserialize_from(decoder)?;

    Ok(movie)
}

impl Subcommand for Dump {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("dump")
            .about("Dumps the internal data of a movie file previously generated with the compile subcommand.")
            .arg(
                Arg::with_name("file")
                    .index(1)
                    .required(true)
                    .help("Movie file to dump"),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        Dump {
            file: PathBuf::from(matches.value_of_os("file").unwrap()),
        }
    }

    fn run(&self) -> Result<(), Box<dyn Error>> {
        let movie = load_movie(&self.file)?;

        println!("{:?}", movie);

        Ok(())
    }
}
