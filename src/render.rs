use crate::animated::Animated;
use crate::dump::load_movie;
use crate::Subcommand;
use cairo::{Context, ImageSurface};
use clap::{App, Arg, ArgMatches};
use std::error::Error;
use std::path::PathBuf;
use crate::encoder::*;

pub enum FormatKind {
    PNG,
    APNG,
    GIF,
    MP4,
}

impl FormatKind {
    pub fn encoder(&self, render: &Render) -> Result<Box<dyn FormatEncoder>, Box<dyn Error>> {
        let ret: Box<dyn FormatEncoder> = match self {
            FormatKind::PNG => Box::new(PNGEncoder::new(render)?),
            FormatKind::APNG => Box::new(APNGEncoder::new(render)?),
            FormatKind::GIF => Box::new(GIFEncoder::new(render)?),
            FormatKind::MP4 => Box::new(MP4Encoder::new(render)?),
        };

        Ok(ret)
    }
}

pub struct Render {
    pub file: PathBuf,
    pub output: PathBuf,
    pub format: FormatKind,
    pub framerate: usize,
    pub resolution: (usize, usize),
}

pub fn parse_resolution(input: &str) -> Result<(usize, usize), Box<dyn Error>> {
    let resolution = match input {
        "fullhd" => (1920, 1080),
        "sd" => (320, 200),
        _ => panic!("oops"),
    };

    Ok(resolution)
}

impl Subcommand for Render {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("render")
            .about("Dumps the internal data of a movie file previously generated with the compile subcommand.")
            .arg(
                Arg::with_name("file")
                    .index(1)
                    .required(true)
                    .help("Movie file to render"),
            )
            .arg(
                Arg::with_name("resolution")
                    .short("r")
                    .long("resolution")
                    .default_value("fullhd")
                    .help("Resolution to render movie at"))
            .arg(
                Arg::with_name("framerate")
                    .long("framerate")
                    .default_value("30")
                    .help("Framerate to render movie at"))
            .arg(
                Arg::with_name("output")
                    .short("o")
                    .long("output")
                    .help("Output file to write to")
                    .required(true)
                    .takes_value(true))
            .arg(
                Arg::with_name("format")
                    .short("f")
                    .long("format")
                    .help("Output format to generate")
                    .default_value("auto"))
    }

    fn parse(matches: &ArgMatches) -> Self {
        let resolution = parse_resolution(matches.value_of("resolution").unwrap()).unwrap();
        let fps = matches
            .value_of("framerate")
            .unwrap()
            .parse::<usize>()
            .unwrap();

        let format = match matches.value_of("format").unwrap() {
            "png" => FormatKind::PNG,
            "apng" => FormatKind::APNG,
            "auto" => FormatKind::PNG,
            "gif" => FormatKind::GIF,
            "mp4" => FormatKind::MP4,
            _ => panic!("oops"),
        };

        Render {
            file: PathBuf::from(matches.value_of_os("file").unwrap()),
            output: PathBuf::from(matches.value_of_os("output").unwrap()),
            format: format,
            resolution: resolution,
            framerate: fps,
        }
    }

    fn run(&self) -> Result<(), Box<dyn Error>> {
        let movie = load_movie(&self.file)?;
        //let mut player = movie.player();

        let mut encoder = self.format.encoder(&self)?;

        let frame_length = 1.0f64 / self.framerate as f64;
        let frames = (movie.duration() / frame_length) as usize;

        let mut surface = ImageSurface::create(
            encoder.image_surface_format(),
            self.resolution.0 as i32,
            self.resolution.1 as i32,
        )
        .map_err(|e| format!("{:?}", e))?;

        for frame in 0..frames {
            println!("[{}/{}] Rendering frame", frame + 1, frames + 1);

            let context = Context::new(&surface);
            context.scale(self.resolution.0 as f64, self.resolution.0 as f64);

            let time = frame as f64 * frame_length;

            context.save();
            movie.draw(&context, time);
            context.restore();

            std::mem::drop(context);

            encoder.encode(&mut surface, frame, time)?;
        }

        println!("[{}/{}] Finishing encoding", frames + 1, frames + 1);
        encoder.finish()?;

        Ok(())
    }
}
