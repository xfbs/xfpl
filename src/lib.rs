pub mod animated;
pub mod common;
pub mod compile;
pub mod drawable;
pub mod dump;
//pub mod items;
pub mod ease;
pub mod movie;
pub mod player;
pub mod render;
pub mod encoder;

use clap::{App, AppSettings, ArgMatches};
pub use common::*;
pub use compile::Compile;
pub use dump::Dump;
pub use player::Player;
pub use render::Render;
use std::error::Error;

trait Subcommand {
    fn app<'a, 'b>() -> App<'a, 'b>;
    fn parse(matches: &ArgMatches) -> Self;
    fn run(&self) -> Result<(), Box<dyn Error>>;
}

pub fn app<'a, 'b>() -> App<'a, 'b> {
    App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .setting(AppSettings::VersionlessSubcommands)
        .subcommand(Player::app())
        .subcommand(Compile::app())
        .subcommand(Dump::app())
        .subcommand(Render::app())
}

pub fn run(matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
    match matches.subcommand() {
        ("play", Some(matches)) => Player::parse(matches).run(),
        ("compile", Some(matches)) => Compile::parse(matches).run(),
        ("dump", Some(matches)) => Dump::parse(matches).run(),
        ("render", Some(matches)) => Render::parse(matches).run(),
        (_, _) => Ok(()),
    }
}

pub fn main() {
    let app = app();
    let matches = app.get_matches();

    match run(&matches) {
        Ok(()) => (),
        Err(e) => println!("{}", e),
    }
}
