use super::*;

pub struct GIFEncoder {
    encoder: ::gif::Encoder<File>,
    delay: u16,
}

impl GIFEncoder {
    pub fn new(render: &Render) -> Result<Self, Box<dyn Error>> {
        let file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(&render.output)?;
        let encoder = ::gif::Encoder::new(file, render.resolution.0 as u16, render.resolution.1 as u16, &[])?;

        Ok(GIFEncoder {
            encoder: encoder,
            delay: (100 / render.framerate) as u16,
        })
    }
}

impl FormatEncoder for GIFEncoder {
    fn encode(
        &mut self,
        surface: &mut ImageSurface,
        _frame: usize,
        _time: f64,
    ) -> Result<(), Box<dyn Error>> {
        let width = surface.get_width() as u16;
        let height = surface.get_height() as u16;

        // let data = surface.get_data()?;
        // let mut value: Vec<u8> = data.deref().into();
        let mut value = get_data(surface)?;
        let mut frame = ::gif::Frame::from_rgb_speed(
            width,
            height,
            &mut value,
            10);
        frame.delay = self.delay;
        self.encoder.write_frame(&frame)?;

        Ok(())
    }

    fn finish(&mut self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    fn image_surface_format(&self) -> cairo::Format {
        cairo::Format::Rgb24
    }
}

