use cairo::ImageSurface;
use std::fs::{File, OpenOptions};
use std::error::Error;
use crate::render::Render;
use std::ops::Deref;

mod gif;
pub use self::gif::GIFEncoder;

mod png;
pub use self::png::PNGEncoder;

mod apng;
pub use self::apng::APNGEncoder;

mod mp4;
pub use self::mp4::MP4Encoder;

pub trait FormatEncoder {
    fn encode(
        &mut self,
        surface: &mut ImageSurface,
        frame: usize,
        time: f64,
    ) -> Result<(), Box<dyn Error>>;
    fn finish(&mut self) -> Result<(), Box<dyn Error>>;
    fn image_surface_format(&self) -> cairo::Format;
}

pub fn get_data(surface: &mut ImageSurface) -> Result<Vec<u8>, Box<dyn Error>> {
    let data = surface.get_data()?;
    let value: Vec<u8> = data
        .deref()
        .chunks(4)
        .map(|chunk| chunk.iter().take(3).rev())
        .flatten()
        .cloned()
        .collect();
    Ok(value)
}
