use super::*;
use ffmpeg_next::software::scaling::{Context as ScalingContext, Flags};
use ffmpeg_next::format::Context;
use ffmpeg_next::util::format::Pixel;
use ffmpeg_next::format::{output_as, stream::Stream};
use ffmpeg_next::codec::{Id as CodecId};
use ffmpeg_next::codec::encoder::{find as find_codec};
use std::convert::TryInto;

pub struct MP4Encoder {
}

impl MP4Encoder {
    pub fn new(render: &Render) -> Result<Self, Box<dyn Error>> {
        ffmpeg_next::format::register_all();

        let scaling_context = ScalingContext::get(
            Pixel::BGR24,
            render.resolution.0.try_into()?,
            render.resolution.1.try_into()?,
            Pixel::YUV420P,
            render.resolution.0.try_into()?,
            render.resolution.1.try_into()?,
            Flags::FAST_BILINEAR)?;

        //let context = Context::Output(scaling_context);

        let mut output = output_as(&render.output, "mp4")?;

        let codec = find_codec(CodecId::H264).ok_or("codec not found")?;

        //let stream = Stream::wrap(&mut output, 0);

        Ok(MP4Encoder {})
    }
}

impl FormatEncoder for MP4Encoder {
    fn encode(
        &mut self,
        _surface: &mut ImageSurface,
        _frame: usize,
        _time: f64,
    ) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    fn finish(&mut self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    fn image_surface_format(&self) -> cairo::Format {
        cairo::Format::Rgb24
    }
}
