use super::*;
use std::path::PathBuf;

pub struct PNGEncoder {
    path: PathBuf,
}

impl PNGEncoder {
    pub fn new(render: &Render) -> Result<Self, Box<dyn Error>> {
        std::fs::create_dir(&render.output)?;

        Ok(PNGEncoder {
            path: render.output.clone(),
        })
    }
}

impl FormatEncoder for PNGEncoder {
    fn encode(
        &mut self,
        surface: &mut ImageSurface,
        frame: usize,
        _: f64,
    ) -> Result<(), Box<dyn Error>> {
        let mut path = self.path.clone();
        path.push(format!("frame{}.png", frame));
        let mut file = OpenOptions::new().write(true).create(true).open(path)?;
        surface.write_to_png(&mut file)?;

        Ok(())
    }

    fn finish(&mut self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    fn image_surface_format(&self) -> cairo::Format {
        cairo::Format::Rgb24
    }
}
