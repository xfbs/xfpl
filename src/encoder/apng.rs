use super::*;

pub struct APNGEncoder {
    file: File,
    frames: Vec<::apng::PNGImage>,
}

impl APNGEncoder {
    pub fn new(render: &Render) -> Result<Self, Box<dyn Error>> {
        let file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(&render.output)?;

        Ok(APNGEncoder {
            file: file,
            frames: Vec::new(),
        })
    }
}

impl FormatEncoder for APNGEncoder {
    fn encode(
        &mut self,
        surface: &mut ImageSurface,
        _frame: usize,
        _time: f64,
    ) -> Result<(), Box<dyn Error>> {
        self.frames.push(::apng::PNGImage {
            width: surface.get_width() as u32,
            height: surface.get_height() as u32,
            data: surface.get_data()?.deref().into(),
            color_type: ::png::ColorType::RGBA,
            bit_depth: ::png::BitDepth::Eight,
        });
        Ok(())
    }

    fn finish(&mut self) -> Result<(), Box<dyn Error>> {
        let config = ::apng::create_config(&self.frames, None).unwrap();

        let mut encoder = ::apng::Encoder::new(&mut self.file, config).unwrap();
        let frame = ::apng::Frame {
            delay_num: Some(1),
            delay_den: Some(2),
            ..Default::default()
        };

        let mut vec = Vec::new();
        std::mem::swap(&mut self.frames, &mut vec);
        match encoder.encode_all(vec, Some(&frame)) {
            Ok(_) => Ok(()),
            Err(e) => Err(e.to_string().into()),
        }
    }

    fn image_surface_format(&self) -> cairo::Format {
        cairo::Format::Rgb24
    }
}
