extern crate gio;
extern crate gtk;

// To import all needed traits.
use gdk::prelude::*;
use gio::prelude::*;
use glib::clone;
use gtk::prelude::*;

use crate::animated::Animated;
use crate::compile::parse_file;
use crate::dump::load_movie;
use crate::movie::Movie;
use crate::Subcommand;
use clap::{App, Arg, ArgMatches};
use rodio::Sink;
use std::cell::RefCell;
use std::error::Error;
use std::io::Cursor;
use std::path::PathBuf;
use std::rc::Rc;
use std::time::Instant;

pub enum Media {
    Script(PathBuf),
    Movie(PathBuf),
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Playback {
    Playing { start: Option<Instant>, offset: f64 },
    Paused { position: f64 },
}

pub struct State {
    playback: Playback,
    movie: Movie,
    frames: usize,
    framerate: usize,
    sink: Sink,
}

impl Playback {
    pub fn paused(position: f64) -> Playback {
        Playback::Paused { position }
    }

    pub fn playing(position: f64) -> Playback {
        Playback::Playing {
            start: None,
            offset: position,
        }
    }

    pub fn time(&mut self) -> f64 {
        match self {
            Playback::Playing { start, offset } => match start {
                None => {
                    *start = Some(Instant::now());
                    *offset
                }
                Some(time) => time.elapsed().as_secs_f64() + *offset,
            },
            Playback::Paused { position } => *position,
        }
    }

    pub fn is_paused(&self) -> bool {
        match self {
            Playback::Playing { .. } => false,
            Playback::Paused { .. } => true,
        }
    }
}

pub struct Player {
    playback: Playback,
    media: Media,
    framerate: usize,
}

impl Subcommand for Player {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("play")
            .arg(
                Arg::with_name("file")
                    .index(1)
                    .required(true)
                    .help("File to play."),
            )
            .arg(
                Arg::with_name("parse")
                    .short("p")
                    .long("parse")
                    .help("Treat input file as script that is to be parsed first."),
            )
            .arg(
                Arg::with_name("framerate")
                    .short("f")
                    .long("framerate")
                    .help("Set framerate of player.")
                    .takes_value(true)
                    .default_value("30")
                    .value_name("FPS"),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        let path = PathBuf::from(matches.value_of_os("file").unwrap());

        let media = match matches.is_present("parse") {
            true => Media::Script(path),
            false => Media::Movie(path),
        };

        let fps = matches
            .value_of("framerate")
            .unwrap()
            .parse::<usize>()
            .unwrap();

        Player {
            media: media,
            framerate: fps,
            playback: Playback::playing(0.0f64),
        }
    }

    fn run(&self) -> Result<(), Box<dyn Error>> {
        let movie = match &self.media {
            Media::Script(path) => parse_file(path.clone())?,
            Media::Movie(path) => load_movie(path)?,
        };

        let device = rodio::default_output_device().unwrap();
        let sink = Sink::new(&device);

        let state = Rc::new(RefCell::new(State {
            movie,
            playback: self.playback,
            frames: 0,
            framerate: self.framerate,
            sink,
        }));

        let app = gtk::Application::new(Some("net.xfbs.player"), gio::ApplicationFlags::FLAGS_NONE)
            .expect("Application::new failed");

        let pause = gio::SimpleAction::new("pause", None);
        let state_clone = state.clone();
        pause.connect_activate(move |_, _| {
            let mut state = state_clone.borrow_mut();
            match state.playback {
                Playback::Paused { .. } => {
                    state.sink.play();
                    state.playback = Playback::playing(state.playback.time())
                }
                Playback::Playing { .. } => {
                    state.sink.pause();
                    state.playback = Playback::paused(state.playback.time())
                }
            };
        });

        app.add_action(&pause);
        app.set_accels_for_action("app.pause", &["space"]);

        let quit = gio::SimpleAction::new("quit", None);
        quit.connect_activate(clone!(@weak app => move |_action, _parameter| {
            app.quit();
        }));
        app.set_accels_for_action("app.quit", &["<Primary>Q"]);
        app.add_action(&quit);

        app.connect_activate(move |app| {
            let cur_state = state.clone();
            let state = state.clone();
            let state2 = state.clone();

            // We create the main window.
            let win = gtk::ApplicationWindow::new(app);

            // Then we set its size and a title.
            win.set_default_size(320, 200);
            win.set_title("Xfbs Player");

            let drawing_area = gtk::DrawingArea::new();
            drawing_area.connect_draw(move |area, cr| {
                let mut state = state.borrow_mut();
                state.frames += 1;

                // normalise drawing size
                let size = area.get_allocation();
                cr.scale(size.width as f64, size.width as f64);

                let elapsed = state.playback.time();

                // draw frame
                cr.save();
                state.movie.draw(cr, elapsed);
                cr.restore();

                if state.frames % 30 == 0 {
                    println!("framerate is {}", state.frames as f64 / elapsed);
                }

                gtk::Inhibit(false)
            });

            win.add(&drawing_area);

            // timeout for redrawing the window. this is how we get a framerate
            // we just tell gtk that everything has been invalidated and needs
            // to be redrawn.
            let framerate = state2.borrow().framerate;
            let drawing_area = drawing_area.clone();
            gtk::timeout_add(1000 / framerate as u32, move || {
                if !state2.borrow().playback.is_paused() {
                    if drawing_area.is_drawable() {
                        match drawing_area.get_window() {
                            Some(window) => window.invalidate_rect(None, true),
                            None => {}
                        }
                    }
                }

                glib::source::Continue(true)
            });

            let cur_state = cur_state.borrow();
            let decoder = rodio::decoder::Decoder::new(Cursor::new(cur_state.movie.get_audio()));
            if let Ok(decoder) = decoder {
                cur_state.sink.append(decoder);
            }

            // Don't forget to make all widgets visible.
            win.show_all();
        });

        app.run(&vec![]);

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
