use super::*;
use crate::common::*;
use cairo::{FontSlant, FontWeight};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ColoredText {
    text: ColoredString,
    font: String,
    size: f64,
}

impl Drawable for ColoredText {
    fn draw(&self, cr: &cairo::Context, _time: f64, width: f64) {
        //cr.set_source_rgba(self.color.red, self.color.green, self.color.blue, self.color.alpha);
        cr.move_to(0.04, 0.05);
        cr.select_font_face(&self.font, FontSlant::Normal, FontWeight::Normal);
        cr.set_font_size(self.size);

        let font_extents = cr.font_extents();

        let mut line_num = 1;
        cr.move_to(0f64, font_extents.height * (line_num as f64));

        for segment in self.text.segments.iter() {
            if segment.content == "\n" {
                line_num += 1;
                cr.move_to(0f64, font_extents.height * (line_num as f64));
                continue;
            }
            let fg = segment.foreground;
            cr.set_source_rgba(fg.red, fg.green, fg.blue, fg.alpha);
            for (i, line) in segment.content.lines().enumerate() {
                if i != 0 {
                    line_num += 1;
                    cr.move_to(0f64, font_extents.height * (line_num as f64));
                }

                cr.show_text(line);
            }
        }

        /*for (i, line) in self.text.lines().enumerate() {
            cr.move_to(0f64, font_extents.height * (i as f64 + 1f64));
            cr.show_text(line);
        }
        */

        /*
        cr.scale(1f64/500f64, 1f64/500f64);

        let layout = pangocairo::create_layout(cr).unwrap();
        layout.set_width(1);
        let mut font_desc = pango::FontDescription::from_string("SF Mono 30");
        //font_desc.set_absolute_size(self.1 * width);
        layout.set_font_description(Some(&font_desc));
        layout.set_markup(&self.0);
        layout.set_single_paragraph_mode(true);
        layout.set_wrap(pango::WrapMode::Word);
        layout.set_justify(false);

        //cr.move_to(100.0f64, 100.0f64);

        pangocairo::update_layout(cr, &layout);
        pangocairo::show_layout(cr, &layout);
        */
    }
}

impl ColoredText {
    pub fn new(text: ColoredString, font: String, size: f64) -> Self {
        ColoredText {
            text: text,
            size: size,
            font: font,
        }
    }
}

#[typetag::serde(name = "coloredtext")]
impl Renderable for RenderItem<ColoredText> {}
