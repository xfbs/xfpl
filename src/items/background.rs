use super::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Background(pub f64, pub f64, pub f64);

impl Background {
    pub fn new(r: f64, g: f64, b: f64) -> Self {
        Background(r, g, b)
    }
}

impl Drawable for Background {
    fn draw(&self, context: &cairo::Context, _: f64, _width: f64) {
        context.set_source_rgb(self.0, self.1, self.2);
        context.paint();
    }
}

#[typetag::serde(name = "background")]
impl Renderable for RenderItem<Background> {}
