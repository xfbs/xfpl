use super::*;
use cairo::{FontSlant, FontWeight};
use serde::{Deserialize, Serialize};
use syntect::easy::HighlightLines;
use syntect::highlighting::{Style, ThemeSet};
use syntect::parsing::SyntaxSet;
use syntect::util::{as_24_bit_terminal_escaped, LinesWithEndings};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Code {
    text: String,
    size: f64,
}

impl Drawable for Code {
    fn draw(&self, cr: &cairo::Context, _time: f64, width: f64) {
        // TODO: lazy static here?
        let ps = SyntaxSet::load_defaults_newlines();
        let ts = ThemeSet::load_defaults();

        let syntax = ps.find_syntax_by_extension("rs").unwrap();
        let mut h = HighlightLines::new(syntax, &ts.themes["base16-ocean.dark"]);

        let markup = self
            .text
            .lines()
            .map(|line| {
                h.highlight(line, &ps)
                    .iter()
                    .map(|(style, code)| {
                        let fg = style.foreground;
                        format!(
                            "<span foreground='#{:02x}{:02x}{:02x}'>{}</span>",
                            fg.r, fg.g, fg.b, code
                        )
                    })
                    .collect::<Vec<String>>()
                    .join("")
            })
            .collect::<Vec<String>>()
            .join("\n");

        let layout = pangocairo::create_layout(cr).unwrap();
        layout.set_width(100000000);
        let mut font_desc = pango::FontDescription::from_string("SF Mono 30");
        font_desc.set_absolute_size(self.size);
        layout.set_font_description(Some(&font_desc));
        layout.set_markup(&markup);

        //cr.move_to(101.0f64, 100.0f64);

        pangocairo::update_layout(cr, &layout);
        pangocairo::show_layout(cr, &layout);
    }
}

impl Code {
    pub fn new(text: String, size: f64) -> Self {
        Code {
            text: text,
            size: size,
        }
    }
}

#[typetag::serde(name = "code")]
impl Renderable for RenderItem<Code> {}
