use clap::{App, Arg, ArgMatches};
use libflate::gzip::Encoder;
use rhai::*;
use std::error::Error;
use std::fs::OpenOptions;
//use std::io::Write;
use std::path::PathBuf;
use std::rc::Rc;

use crate::animated;
use crate::common;
use crate::drawable;
use crate::ease;
use crate::movie::Movie;
use crate::Subcommand;

use crate::animated::*;
use crate::common::*;
use crate::drawable::*;

use std::io::Cursor;
use std::time::Instant;

pub struct Compile {
    input: PathBuf,
    output: PathBuf,
    verbose: bool,
}

impl Subcommand for Compile {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("compile")
            .arg(
                Arg::with_name("output")
                    .short("o")
                    .long("output")
                    .value_name("FILE")
                    .help("File to write compiled output into.")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("verbose")
                    .short("v")
                    .long("verbose")
                    .help("Enable more verbose output."),
            )
            .arg(
                Arg::with_name("input")
                    .index(1)
                    .required(true)
                    .help("Script to compile."),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        Compile {
            input: PathBuf::from(matches.value_of_os("input").unwrap()),
            output: PathBuf::from(matches.value_of_os("output").unwrap()),
            verbose: matches.is_present("verbose"),
        }
    }

    fn run(&self) -> Result<(), Box<dyn Error>> {
        let start = Instant::now();
        let movie = parse_file(self.input.clone())?;

        if self.verbose {
            println!("parsed in {}s", start.elapsed().as_secs_f64());
        }

        let mut output = Vec::new();
        let mut cursor = Cursor::new(&mut output);

        //bincode::serialize_into(&mut encoder, &movie)?;

        let start = Instant::now();
        bincode::serialize_into(&mut cursor, &movie)?;

        if self.verbose {
            println!("serialised in {}s", start.elapsed().as_secs_f64());
        }

        let mut cursor = Cursor::new(&mut output);

        let file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&self.output)?;

        let mut encoder = Encoder::new(file)?;

        let start = Instant::now();
        std::io::copy(&mut cursor, &mut encoder)?;
        encoder.finish().into_result()?;

        if self.verbose {
            println!("written in {}s", start.elapsed().as_secs_f64());
        }

        Ok(())
    }
}

pub fn load_file(path: String) -> Result<Rc<Vec<u8>>, Box<rhai::EvalAltResult>> {
    std::fs::read(path)
        .map_err(|e| e.to_string().into())
        .map(|s| Rc::new(s))
}

pub fn open_file(path: String) -> Result<Rc<std::fs::File>, Box<rhai::EvalAltResult>> {
    std::fs::OpenOptions::new()
        .read(true)
        .open(path)
        .map_err(|e| e.to_string().into())
        .map(|f| Rc::new(f))
}

pub fn engine() -> Engine {
    let mut engine = Engine::new();

    engine.register_type::<ease::Ease>();
    engine.register_fn("ease_linear", || ease::Ease::Linear);
    engine.register_fn("ease_sine", || ease::Ease::Sine);
    engine.register_fn("ease_quad", || ease::Ease::Quad);
    engine.register_fn("ease_cubic", || ease::Ease::Cubic);

    engine.register_type::<common::RGBA>();
    engine.register_fn("rgba", common::RGBA::new);

    engine.register_type::<common::ColoredSegment>();
    engine.register_fn("colored_segment", common::ColoredSegment::new);
    engine.register_fn("set_foreground", common::ColoredSegment::set_foreground);
    engine.register_fn("set_background", common::ColoredSegment::set_background);

    engine.register_type::<common::ColoredString>();
    engine.register_fn("colored_string", common::ColoredString::new);
    engine.register_fn("add", ColoredString::add);

    engine.register_result_fn("highlight", highlight);

    engine.register_type::<Rc<Vec<u8>>>();
    engine.register_result_fn("load_file", load_file);

    engine.register_type::<Rc<std::fs::File>>();
    engine.register_result_fn("open_file", open_file);

    engine.register_type::<Box<dyn Drawable>>();
    engine.register_type::<Box<dyn Animated>>();

    engine.register_type::<common::Size>();
    engine.register_fn("size", common::Size::new);

    drawable::ColoredText::register(&mut engine);
    drawable::Text::register(&mut engine);
    drawable::Bounded::register(&mut engine);
    drawable::Clipped::register(&mut engine);
    drawable::Fill::register(&mut engine);
    drawable::SVG::register(&mut engine);
    drawable::Nothing::register(&mut engine);
    drawable::Scaled::register(&mut engine);

    animated::register_all(&mut engine);

    Movie::register(&mut engine);

    engine
}

pub fn parse_file(file: PathBuf) -> Result<Movie, Box<EvalAltResult>> {
    let engine = engine();
    engine.eval_file::<Movie>(file)
}
