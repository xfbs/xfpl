use crate::drawable::Drawable;

pub trait Renderable {
    fn start_visible(&self) -> f64;
    fn end_visible(&self) -> Option<f64>;
}

pub struct RenderItem<T> where T: Drawable {
    start: f64,
    end: Option<f64>,
    drawable: T,
}

impl<T> Drawable for RenderItem<T> where T: Drawable {
    fn draw(&self, context: &cairo::Context, time: f64) {
        self.drawable.draw(context, time);
    }
}

impl<T> Renderable for RenderItem<T> where T: Drawable {
    fn start_visible(&self) -> f64 {
        self.start
    }

    fn end_visible(&self) -> Option<f64> {
        self.end
    }
}
