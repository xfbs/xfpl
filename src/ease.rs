use serde::{Deserialize, Serialize};

/// Represents an easing function, see <https://easings.net>. This is used to
/// provide smooth, natural movement.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Ease {
    Linear,
    Sine,
    Quad,
    Cubic,
}

// FIXME: this isn't the right function
fn ease_sine(x: f64) -> f64 {
    -0.5 + 2.0 * x + 2.0 * (0.5 - x).powi(2) * (0.5 - x).signum()
}

fn ease_quad(x: f64) -> f64 {
    if x < 0.5 {
        2.0 * x.powi(2)
    } else {
        1f64 - (-2.0f64 * x + 2.0).powi(2) / 2.0
    }
}

fn ease_cubic(x: f64) -> f64 {
    if x < 0.5 {
        4.0 * x.powi(3)
    } else {
        1.0 - (-2.0 * x + 2.0).powi(3) / 2.0
    }
}

impl Ease {
    pub fn ease(&self, x: f64) -> f64 {
        use Ease::*;
        match self {
            Linear => x,
            Sine => ease_sine(x),
            Quad => ease_quad(x),
            Cubic => ease_cubic(x),
        }
    }
}

impl Default for Ease {
    fn default() -> Ease {
        Ease::Linear
    }
}

#[test]
fn test_ease() {
    fn test_limits(ease: Ease) {
        assert!(ease.ease(0.0).abs() < 0.001);
        assert!((1.0 - ease.ease(1.0)).abs() < 0.001);
    }

    test_limits(Ease::Linear);
    test_limits(Ease::Sine);
    test_limits(Ease::Quad);
    test_limits(Ease::Cubic);
}
