pub mod background;
pub mod code;
pub mod colored_text;
pub mod text;

pub use background::Background;
pub use code::Code;
pub use colored_text::ColoredText;
pub use text::Text;

use std::fmt::Debug;

use serde::{Deserialize, Serialize};

pub trait Drawable {
    fn draw(&self, context: &cairo::Context, time: f64, width: f64);
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Size {
    pub x: f64,
    pub y: f64,
}

pub trait Timed {
    fn start(&self) -> f64;
    fn end(&self) -> Option<f64>;
}

#[typetag::serde(tag = "type")]
pub trait Renderable: Drawable + Timed + std::fmt::Debug + CloneRenderable {}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct RenderItem<T>
where
    T: Drawable + Clone + std::fmt::Debug + Serialize,
{
    start: f64,
    end: Option<f64>,
    drawable: T,
}

impl<'a, T> Drawable for RenderItem<T>
where
    T: Drawable + Clone + std::fmt::Debug + Serialize + Deserialize<'a>,
{
    fn draw(&self, context: &cairo::Context, time: f64, width: f64) {
        self.drawable.draw(context, time, width);
    }
}

impl<'a, T> Timed for RenderItem<T>
where
    T: Drawable + Clone + std::fmt::Debug + Serialize + Deserialize<'a>,
{
    fn start(&self) -> f64 {
        self.start
    }

    fn end(&self) -> Option<f64> {
        self.end
    }
}

//impl<'a, T> Renderable for RenderItem<T> where T: Drawable + std::fmt::Debug + Clone + Serialize + Deserialize<'a> + 'static {}

impl<'a, T> RenderItem<T>
where
    T: Drawable + Clone + std::fmt::Debug + Serialize + Deserialize<'a>,
{
    pub fn new_bounded(item: T, start: f64, end: f64) -> Self {
        RenderItem {
            start: start,
            end: Some(end),
            drawable: item,
        }
    }

    pub fn new_unbounded(item: T, start: f64) -> Self {
        RenderItem {
            start: start,
            end: None,
            drawable: item,
        }
    }
}

pub trait CloneRenderable {
    fn clone_boxed(&self) -> Box<dyn Renderable>;
}

impl<T> CloneRenderable for T
where
    T: Renderable + Clone + 'static,
{
    fn clone_boxed(&self) -> Box<dyn Renderable> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Renderable> {
    fn clone(&self) -> Self {
        (*self).clone_boxed()
    }
}

#[test]
fn test_clone_boxed() {
    RenderItem::new_bounded(Background::new(0.0, 0.0, 0.0), 0.0, 1.0).clone_boxed();
}
