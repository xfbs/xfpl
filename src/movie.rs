//use crate::items::*;
use crate::animated::Animated;
use crate::common::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io::{self, Cursor, Read};
use std::rc::Rc;

/// A Movie object holds an Animation object as well as some metadata. It
/// implements Animated itself, but just passes all calls down to the object.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Movie {
    title: Option<String>,
    animated: Box<dyn Animated>,
    audio: Vec<u8>,
    audio_files: HashMap<String, Vec<u8>>,
    audio_items: Vec<AudioItem>,
}

impl Movie {
    pub fn new(item: Box<dyn Animated>) -> Movie {
        Movie {
            title: None,
            animated: item,
            audio: Vec::new(),
            audio_files: HashMap::new(),
            audio_items: Vec::new(),
        }
    }

    pub fn set_title<T: Into<String>>(&mut self, title: T) {
        self.title = Some(title.into());
    }

    pub fn set_audio(&mut self, audio: Rc<Vec<u8>>) {
        self.audio = audio.as_ref().to_vec();
    }

    pub fn get_audio(&self) -> Vec<u8> {
        self.audio.clone()
    }

    pub fn add_audio<S: Into<String> + Clone, R: Read>(
        &mut self,
        name: S,
        from: &mut R,
    ) -> io::Result<()> {
        let name = name.into();
        self.audio_files.insert(name.clone(), Vec::new());

        let item = self.audio_files.get_mut(&name).unwrap();
        let mut cursor = Cursor::new(item);

        std::io::copy(from, &mut cursor)?;
        Ok(())
    }

    pub fn rhai_add_audio(
        &mut self,
        name: String,
        from: Rc<std::fs::File>,
    ) -> Result<(), Box<rhai::EvalAltResult>> {
        from.try_clone()
            .map_err(|e| e.to_string().into())
            .and_then(|mut f| {
                self.add_audio(name, &mut f)
                    .map_err(|e| e.to_string().into())
            })
    }

    pub fn play_audio(&mut self, name: &str, time: f64) {
        let mut item = AudioItem::new(name);
        item.set_start(time);
        self.audio_items.push(item);
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        use rhai::RegisterResultFn;
        engine.register_type::<Movie>();
        engine.register_fn("set_title", Movie::set_title::<String>);
        engine.register_fn("set_audio", Movie::set_audio);
        engine.register_result_fn("add_audio", Movie::rhai_add_audio);
        //engine.register_result_fn("add_audio", Movie::add_audio::<String, Rc<std::fs::File>>);
        engine.register_fn("movie", Movie::new);
    }
}

#[typetag::serde(name = "movie")]
impl Animated for Movie {
    fn draw(&self, context: &cairo::Context, offset: f64) {
        self.animated.draw(context, offset);
    }

    fn size(&self) -> Size {
        self.animated.size()
    }

    fn duration(&self) -> f64 {
        self.animated.duration()
    }

    fn audio(&self) -> Box<dyn Iterator<Item = AudioItem>> {
        self.animated.audio()
    }
}
