use super::*;

/// Drawable item that doesn't draw anything.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Nothing;

impl Nothing {
    pub fn new() -> Self {
        Nothing
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Nothing>();
        engine.register_fn("nothing", Nothing::new);
        engine.register_fn("as_drawable", Nothing::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for Nothing {
    fn draw(&self, _context: &cairo::Context) {}

    fn size(&self) -> Size {
        Size::default()
    }
}

#[test]
fn test_serialize() {
    let item = Nothing::new();

    let serialized = bincode::serialize(&item).unwrap();
    let item_re = bincode::deserialize(&serialized).unwrap();

    assert_eq!(item, item_re);

    let serialized = bincode::serialize(&item.to_drawable()).unwrap();
    let item_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(item_re.size(), Size::default());
}
