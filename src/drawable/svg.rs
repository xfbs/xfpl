use super::*;
use crate::common::*;
use serde::{Deserialize, Serialize};
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Clone, Serialize, Deserialize)]
pub struct SVG {
    data: String,

    #[serde(skip)]
    parsed: RefCell<Option<resvg::usvg::Tree>>,
}

impl SVG {
    pub fn new<S: Into<String>>(data: S) -> SVG {
        SVG {
            data: data.into(),
            parsed: Default::default(),
        }
    }

    pub fn from_file(file: Rc<std::fs::File>) -> Result<SVG, Box<rhai::EvalAltResult>> {
        use std::io::Read;
        file.try_clone()
            .map_err(|e| e.to_string().into())
            .and_then(|mut f| {
                let mut s = String::new();
                f.read_to_string(&mut s)
                    .map_err(|e| e.to_string().into())
                    .map(|_| s)
            })
            .map(|data| SVG { data, parsed: Default::default() })
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        use rhai::RegisterResultFn;
        engine.register_type::<SVG>();
        engine.register_fn("svg", SVG::new::<String>);
        engine.register_result_fn("svg", SVG::from_file);
        engine.register_fn("as_drawable", SVG::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for SVG {
    fn draw(&self, cr: &cairo::Context) {
        if self.parsed.borrow().is_none() {
            let opt = resvg::Options::default();
            let tree = resvg::usvg::Tree::from_str(&self.data, &opt.usvg).unwrap();
            *self.parsed.borrow_mut() = Some(tree);
        }

        let parsed = self.parsed.borrow();
        let tree: &resvg::usvg::Tree = parsed.as_ref().unwrap();
        let opt = resvg::Options::default();

        let s = resvg::ScreenSize::new(1, 1).unwrap();
        resvg::backend_cairo::render_to_canvas(tree, &opt, s, cr);
    }

    fn size(&self) -> Size {
        Size::new(1f64, 1f64)
    }
}

impl std::fmt::Debug for SVG {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SVG")
         .field("data", &self.data)
         .finish()
    }
}
