use super::*;
use crate::common::*;
use cairo::{FontSlant, FontWeight};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Text {
    color: RGBA,
    text: String,
    font: String,
    size: f64,
}

#[typetag::serde]
impl Drawable for Text {
    fn draw(&self, cr: &cairo::Context) {
        cr.set_source_rgba(
            self.color.red,
            self.color.green,
            self.color.blue,
            self.color.alpha,
        );
        cr.move_to(0.04, 0.05);
        cr.select_font_face(&self.font, FontSlant::Normal, FontWeight::Normal);
        cr.set_font_size(self.size);

        let font_extents = cr.font_extents();

        for (i, line) in self.text.lines().enumerate() {
            cr.move_to(0f64, font_extents.height * (i as f64 + 1f64));
            cr.show_text(line);
        }
    }

    fn size(&self) -> Size {
        Size::new(1f64, 1f64)
    }
}

impl Text {
    pub fn new(text: String, font: String, size: f64, color: RGBA) -> Self {
        Text {
            color: color,
            text: text,
            size: size,
            font: font,
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Text>();
        engine.register_fn("text", Text::new);
        engine.register_fn("as_drawable", Text::as_drawable);
    }
}

#[test]
fn test_serialize() {
    let black = RGBA::new(0.0, 0.0, 0.0, 0.0);
    let text = Text::new("hello".into(), "arial".into(), 1.0, black);

    let serialized = bincode::serialize(&text).unwrap();
    let text_re = bincode::deserialize(&serialized).unwrap();

    assert_eq!(text, text_re);

    let serialized = bincode::serialize(&text.to_drawable()).unwrap();
    let text_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(text_re.size(), Size::new(1f64, 1f64));
}
