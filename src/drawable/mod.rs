pub mod colored_text;
pub mod text;
pub mod nothing;
mod fill;
mod scaled;
mod bounded;
mod clipped;
mod svg;
pub use colored_text::ColoredText;
pub use text::Text;
pub use nothing::Nothing;
pub use fill::Fill;
pub use scaled::Scaled;
pub use bounded::Bounded;
pub use clipped::Clipped;
pub use svg::SVG;

use crate::common::*;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

/// Represents something that can be drawn on a Cairo surface. This must be
/// something that doesn't move or change, otherwise use the Animated trait.
#[typetag::serde(tag = "draw")]
pub trait Drawable: CloneDrawable + Debug {
    fn draw(&self, context: &cairo::Context);
    fn size(&self) -> Size;
}

pub trait CloneDrawable {
    fn to_drawable(&self) -> Box<dyn Drawable>;
    fn as_drawable(self) -> Box<dyn Drawable>;
}

impl<T> CloneDrawable for T
where
    T: Drawable + Clone + 'static,
{
    fn to_drawable(&self) -> Box<dyn Drawable> {
        Box::new(self.clone())
    }

    fn as_drawable(self) -> Box<dyn Drawable> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Drawable> {
    fn clone(&self) -> Self {
        (*self).to_drawable()
    }
}
