use super::*;
use crate::common::*;
use cairo::{FontSlant, FontWeight};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ColoredText {
    text: ColoredString,
    font: String,
    size: f64,
}

#[typetag::serde]
impl Drawable for ColoredText {
    fn draw(&self, cr: &cairo::Context) {
        cr.save();
        cr.move_to(0.04, 0.05);
        cr.select_font_face(&self.font, FontSlant::Normal, FontWeight::Normal);
        cr.set_font_size(self.size);

        let font_extents = cr.font_extents();

        let mut line_num = 1;
        cr.move_to(0f64, font_extents.height * (line_num as f64));

        for segment in self.text.segments.iter() {
            if segment.content == "\n" {
                line_num += 1;
                cr.move_to(0f64, font_extents.height * (line_num as f64));
                continue;
            }
            let fg = segment.foreground;
            cr.set_source_rgba(fg.red, fg.green, fg.blue, fg.alpha);
            for (i, line) in segment.content.lines().enumerate() {
                if i != 0 {
                    line_num += 1;
                    cr.move_to(0f64, font_extents.height * (line_num as f64));
                }

                cr.show_text(line);
            }
        }

        cr.restore();
    }

    fn size(&self) -> Size {
        Size::new(1f64, 1f64)
    }
}

impl ColoredText {
    pub fn new(text: ColoredString, font: String, size: f64) -> Self {
        ColoredText {
            text: text,
            size: size,
            font: font,
        }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<ColoredText>();
        engine.register_fn("colored_text", ColoredText::new);
        engine.register_fn("as_drawable", ColoredText::as_drawable);
    }
}

#[test]
fn test_serialize() {
    let string = ColoredString::new();
    let text = ColoredText::new(string, "arial".into(), 1.0);

    let serialized = bincode::serialize(&text).unwrap();
    let text_re = bincode::deserialize(&serialized).unwrap();

    assert_eq!(text, text_re);

    let serialized = bincode::serialize(&text.to_drawable()).unwrap();
    let text_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(text_re.size(), Size::new(1f64, 1f64));
}
