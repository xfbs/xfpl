use super::*;

/// Scales an item by a given factor.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Scaled {
    item: Box<dyn Drawable>,
    scale: f64,
}

impl Scaled {
    pub fn new(item: Box<dyn Drawable>, scale: f64) -> Scaled {
        Scaled { item, scale }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Scaled>();
        engine.register_fn("scaled", Scaled::new);
        engine.register_fn("as_drawable", Scaled::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for Scaled {
    fn draw(&self, context: &cairo::Context) {
        context.save();
        context.scale(self.scale, self.scale);
        self.item.draw(context);
        context.restore();
    }

    fn size(&self) -> Size {
        self.item.size().scaled(self.scale)
    }
}

#[test]
fn test_serialize() {
    let item = Nothing::new();
    let item = Scaled::new(item.as_drawable(), 0.5);

    let serialized = bincode::serialize(&item).unwrap();
    let item_re: Scaled = bincode::deserialize(&serialized).unwrap();

    assert_eq!(item.size(), item_re.size());

    let serialized = bincode::serialize(&item.to_drawable()).unwrap();
    let item_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(item_re.size(), item.size());
}
