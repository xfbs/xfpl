use super::*;

/// Clip a drawable item to a given size. This basically crops the item to the
/// given size, discarding anything that is not within bounds.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Clipped {
    item: Box<dyn Drawable>,
}

impl Clipped {
    pub fn new(item: Box<dyn Drawable>) -> Self {
        Clipped { item }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Clipped>();
        engine.register_fn("clipped", Clipped::new);
        engine.register_fn("as_drawable", Clipped::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for Clipped {
    fn draw(&self, context: &cairo::Context) {
        context.save();
        let size = self.item.size();
        context.rectangle(0f64, 0f64, size.x, size.y);
        context.clip();
        self.item.draw(context);
        context.restore();
    }

    fn size(&self) -> Size {
        self.item.size()
    }
}

#[test]
fn test_serialize() {
    let item = Nothing::new();
    let item = Clipped::new(item.as_drawable());

    let serialized = bincode::serialize(&item).unwrap();
    let item_re: Clipped = bincode::deserialize(&serialized).unwrap();

    assert_eq!(item.size(), item_re.size());

    let serialized = bincode::serialize(&item.to_drawable()).unwrap();
    let item_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(item_re.size(), item.size());
}
