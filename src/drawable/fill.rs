use super::*;

/// Fill everything with a solid colour.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Fill {
    color: RGBA,
}

impl Fill {
    pub fn new(color: RGBA) -> Self {
        Fill { color }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Fill>();
        engine.register_fn("fill", Fill::new);
        engine.register_fn("as_drawable", Fill::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for Fill {
    fn draw(&self, context: &cairo::Context) {
        context.set_source_rgba(
            self.color.red,
            self.color.green,
            self.color.blue,
            self.color.alpha,
        );
        context.paint();
    }

    fn size(&self) -> Size {
        Size::new(1f64, 1f64)
    }
}

#[test]
fn test_serialize() {
    let black = RGBA::new(0.0, 0.0, 0.0, 0.0);
    let item = Fill::new(black);

    let serialized = bincode::serialize(&item).unwrap();
    let item_re = bincode::deserialize(&serialized).unwrap();

    assert_eq!(item, item_re);

    let serialized = bincode::serialize(&item.to_drawable()).unwrap();
    let item_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(item_re.size(), item.size());
}
