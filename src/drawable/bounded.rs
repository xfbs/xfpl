use super::*;

/// Assigns bounds to a drawable item. This causes the item to have a different
/// apparent size (but does not actually clip the size of the item, use Clipped
/// for that).
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Bounded {
    item: Box<dyn Drawable>,
    size: Size,
}

impl Bounded {
    pub fn new(item: Box<dyn Drawable>, size: Size) -> Self {
        Bounded { item, size }
    }

    pub fn register(engine: &mut rhai::Engine) {
        use rhai::RegisterFn;
        engine.register_type::<Bounded>();
        engine.register_fn("bounded", Bounded::new);
        engine.register_fn("as_drawable", Bounded::as_drawable);
    }
}

#[typetag::serde]
impl Drawable for Bounded {
    fn draw(&self, context: &cairo::Context) {
        self.item.draw(context);
    }

    fn size(&self) -> Size {
        self.size
    }
}

#[test]
fn test_serialize() {
    let item = Nothing::new();
    let item = Bounded::new(item.as_drawable(), Size::default());

    let serialized = bincode::serialize(&item).unwrap();
    let item_re: Bounded = bincode::deserialize(&serialized).unwrap();

    assert_eq!(item.size(), item_re.size());

    let serialized = bincode::serialize(&item.to_drawable()).unwrap();
    let item_re: Box<dyn Drawable> = bincode::deserialize(&serialized).unwrap();

    // can't test for equality as of yet
    assert_eq!(item_re.size(), item.size());
}
