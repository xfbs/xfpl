FROM rust

RUN apt update && apt install -y libatk1.0-dev libpango1.0-dev libasound2-dev pkg-config libgdk-pixbuf2.0-dev libgtk-3-dev cmake llvm clang linux-perf && rm -rf /var/cache/apt/list/*
RUN cargo install flamegraph && cargo install cargo-bloat && cargo install cargo-tree && rm -rf /usr/local/cargo/registry
